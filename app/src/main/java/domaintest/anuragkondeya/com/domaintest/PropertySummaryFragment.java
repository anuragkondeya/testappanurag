package domaintest.anuragkondeya.com.domaintest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * Fragment displays list of property received from server
 */
public class PropertySummaryFragment extends Fragment implements Constants {

    private OnFragmentInteractionListener mListener;
    private static ArrayList<Property> mPropertyList = null;
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Property property);
    }

    private void displayAlertDialog(String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.domainAlertDialog);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();

            }
        });
        builder.create().show();
    }

    private AdapterView.OnItemClickListener propertySelect() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (null != mListener) {
                    Property property = mPropertyList.get(position);
                    mListener.onFragmentInteraction(property);
                }
            }
        };
    }

    class FetchPropertyListings extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        private void parseListingJSON(String answer) {
            try {
                mPropertyList = new ArrayList<Property>();
                JSONObject baseJSONObject = new JSONObject(answer);
                JSONObject listingResults = baseJSONObject.getJSONObject(LISTINGRESULTS);
                if (null != listingResults) {
                    JSONArray listingArray = listingResults.getJSONArray(LISTINGS);
                    for (int i = 0; i < listingArray.length(); i++) {
                        JSONObject listing = listingArray.getJSONObject(i);

                        //TODO reuse same object here
                        Property property = new Property();
                        property.setAdId(listing.getInt(ADID));
                        property.setAgencyLogoUrl(listing.getString(AGENCY_LOGO_URL));
                        property.setBathRooms(listing.getInt(BATHROOMS));
                        property.setBedrooms(listing.getInt(BEDROOMS));
                        property.setCarspace(listing.getInt(CARSPACE));
                        property.setIsElite(listing.getInt(ISELITE));
                        property.setDisplayPrice(listing.getString(DISPLAY_PRICE));
                        property.setDisplayAddress(listing.getString(DISPLAYABLE_ADDRESS));
                        property.setTruncatedDescription(listing.getString(TRUNCATED_DESCRIPTION));
                        property.setRetinaDisplayThumbURL(listing.getString(RETINA_DISPLAY_URL));
                        property.setSecondRetinaDisplayThumbURL(listing.getString(SECOND_RETINA_DISPLAY_URL));
                        mPropertyList.add(property);
                        property = null;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Activity activity = getActivity();
            String title = activity.getResources().getString(R.string.progressdialog_title);
            String message = activity.getResources().getString(R.string.progressdialog_message);
            progressDialog = ProgressDialog.show(getActivity(),title,message, true);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            ListView mListView = (ListView) getActivity().findViewById(R.id.listing_summary);
            final ListingAdapter adapter = new ListingAdapter(getActivity(), mPropertyList);
            mListView.setOnItemClickListener(propertySelect());
            mListView.setAdapter(adapter);
            if (getActivity().findViewById(R.id.property_detail) != null)
            mListener.onFragmentInteraction(mPropertyList.get(0));


        }

        @Override
        protected Void doInBackground(Void... voids) {
            String mode;
            String sub;
            int pcode;
            String state;
            SharedPreferences settingValues = getActivity().getSharedPreferences(SETTING_PREFERENCE_MAIN, Context.MODE_PRIVATE);
            if (null != settingValues) {
                mode = settingValues.getString(SETTINGS_KEY_MODE, URL_QPARMA_MODE_DEFAULT);
                sub = settingValues.getString(SETTINGS_KEY_SUB, URL_QPARMA_SUB_DEFAULT);
                pcode = settingValues.getInt(SETTINGS_KEY_PCODE, URL_QPARMA_PCODE_DEFAULT);
                state = settingValues.getString(SETTINGS_KEY_STATE, URL_QPARMA_STATE_DEFAULT);

                Uri.Builder builder = new Uri.Builder();
                builder.scheme(URL_SCHEME_HTTPS);
                builder.authority(URL_AUTHORITY);
                builder.appendPath(URL_PATH_SEARCH_SERVICE);
                builder.appendPath(URL_PATH_MAP_SEARCH);
                builder.appendQueryParameter(URL_QPARAM_MODE, mode);
                builder.appendQueryParameter(URL_QPARMA_SUBURB, sub);
                try {
                    builder.appendQueryParameter(URL_QPARAM_PINCODE, Integer.toString(pcode));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    return null;
                }
                builder.appendQueryParameter(URL_QPARAM_STATE, state);
                try {
                    URL url = new URL(builder.build().toString());
                    if (null != url) {
                        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                        connection.setRequestMethod("GET");
                        BufferedReader input = new BufferedReader(
                                new InputStreamReader(connection.getInputStream()));
                        String answer = input.readLine();
                        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            parseListingJSON(answer);
                        } else {
                            //TODO return error message
                        }
                        input.close();
                    }
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
    private boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == connectivityManager)
            return false;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (null == networkInfo)
            return false;
        else
            return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isNetworkConnected(getActivity().getApplicationContext())){
            displayAlertDialog(getResources().getString(R.string.no_internet_title),getResources().getString(R.string.no_internet_message));
        }
        if (null == mPropertyList) {
            FetchPropertyListings fetchNewListing = new FetchPropertyListings();
            fetchNewListing.execute();
        } else {
            if (0 == mPropertyList.size()) {
                FetchPropertyListings fetchNewListing = new FetchPropertyListings();
                fetchNewListing.execute();
            } else {
                ListView mListView = (ListView) getActivity().findViewById(R.id.listing_summary);
                final ListingAdapter adapter = new ListingAdapter(getActivity(), mPropertyList);
                mListView.setOnItemClickListener(propertySelect());
                mListView.setAdapter(adapter);
            }
        }
    }

    public void saveToFavorites(View view) {
        //TODO : Add code for favorites

    }

    public PropertySummaryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_property_summary, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}

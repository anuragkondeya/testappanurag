package domaintest.anuragkondeya.com.domaintest;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * Class to hold a property object
 */
public class Property implements Parcelable {

    private int mAdId = -1;
    private String mAgencyLogoUrl=" ";
    private int mBathRooms=-1;
    private int mBedrooms=-1;
    private int mCarspace=-1;
    private String mDisplayPrice=" ";
    private String mDisplayableAddress=" ";;
    private String mTruncatedDescription=" ";;
    private String mRetinaDisplayThumbURL=" ";;
    private String mSecondRetinaDisplayThumbURL=" ";;
    private int mIsElite=-1;

    private Bitmap mAgencyLogoBitmap;
    private Bitmap mRetinaDisplayBitmap;
    private Bitmap mSecondRetinaDisplayBitmap;

    public Property(){

    };

    protected Property(Parcel in) {
        mAdId = in.readInt();
        mAgencyLogoUrl = in.readString();
        mBathRooms = in.readInt();
        mBedrooms = in.readInt();
        mCarspace = in.readInt();
        mDisplayPrice = in.readString();
        mDisplayableAddress = in.readString();
        mTruncatedDescription = in.readString();
        mRetinaDisplayThumbURL = in.readString();
        mSecondRetinaDisplayThumbURL = in.readString();
        mIsElite = in.readInt();
        mAgencyLogoBitmap = in.readParcelable(Bitmap.class.getClassLoader());
        mRetinaDisplayBitmap = in.readParcelable(Bitmap.class.getClassLoader());
        mSecondRetinaDisplayBitmap = in.readParcelable(Bitmap.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mAdId);
        dest.writeString(mAgencyLogoUrl);
        dest.writeInt(mBathRooms);
        dest.writeInt(mBedrooms);
        dest.writeInt(mCarspace);
        dest.writeString(mDisplayPrice);
        dest.writeString(mDisplayableAddress);
        dest.writeString(mTruncatedDescription);
        dest.writeString(mRetinaDisplayThumbURL);
        dest.writeString(mSecondRetinaDisplayThumbURL);
        dest.writeInt(mIsElite);
        dest.writeParcelable(mAgencyLogoBitmap, flags);
        dest.writeParcelable(mRetinaDisplayBitmap, flags);
        dest.writeParcelable(mSecondRetinaDisplayBitmap, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Property> CREATOR = new Creator<Property>() {
        @Override
        public Property createFromParcel(Parcel in) {
            return new Property(in);
        }

        @Override
        public Property[] newArray(int size) {
            return new Property[size];
        }
    };

    public  Bitmap getBitmapImageFromBytes(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    private byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    public void setAdId(int id) { this.mAdId = id;}

    public void setAgencyLogoUrl(String agencyLogo) {
        this.mAgencyLogoUrl = agencyLogo;
        if(null!=agencyLogo){
            try {
                java.net.URL url = new java.net.URL(agencyLogo);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                mAgencyLogoBitmap = BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                e.printStackTrace();
                return ;
            }
        }
    }

    public void setBathRooms(int bathrooms) {
        this.mBathRooms = bathrooms;
    }

    public void setBedrooms(int bedrooms) {
        this.mBedrooms = bedrooms;
    }

    public void setCarspace(int carspace) {
        this.mCarspace = carspace;
    }

    public void setDisplayPrice(String displayPrice) {
        this.mDisplayPrice = displayPrice;
    }

    public void setDisplayAddress(String displayAddress) { this.mDisplayableAddress = displayAddress;}

    public void setTruncatedDescription(String description) { this.mTruncatedDescription = description; }

    public void setRetinaDisplayThumbURL(String retinaDisplayThumbURL) {
        this.mRetinaDisplayThumbURL = retinaDisplayThumbURL;
        if(null!=retinaDisplayThumbURL){
            try {
                java.net.URL url = new java.net.URL(retinaDisplayThumbURL);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                mRetinaDisplayBitmap = BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                e.printStackTrace();
                return ;
            }
        }

    }

    public void setSecondRetinaDisplayThumbURL(String secondRetinaDisplayThumbURL) {
        this.mSecondRetinaDisplayThumbURL = secondRetinaDisplayThumbURL;
        if(null!=secondRetinaDisplayThumbURL){
            try {
                java.net.URL url = new java.net.URL(secondRetinaDisplayThumbURL);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                mSecondRetinaDisplayBitmap = BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                e.printStackTrace();
                return ;
            }
        }
    }

    public void setIsElite(int isElite) {
        this.mIsElite = isElite;
    }

    public int getAdId(){ return this.mAdId;}

    public String getAgencyLogoUrl() {
        return this.mAgencyLogoUrl;
    }

    public int getBathRooms() {
        return this.mBathRooms;
    }

    public int getBedrooms() {
        return this.mBedrooms;
    }

    public int getCarspace() {
        return this.mCarspace;
    }

    public String getDisplayPrice() {
        return this.mDisplayPrice;
    }

    public String getDisplayAddress() {
        return this.mDisplayableAddress;
    }

    public String getTruncatedDescription() {
        return this.mTruncatedDescription;
    }

    public String getRetinaDisplayThumbURL() {
        return this.mRetinaDisplayThumbURL;
    }

    public String getSecondRetinaDisplayThumbURL() {
        return this.mSecondRetinaDisplayThumbURL;
    }

    public int getIsElite() {
        return this.mIsElite;
    }

    public Bitmap getAgencyLogoBitmap(){return mAgencyLogoBitmap;}

    public Bitmap getRetinaDisplayBitmap(){return mRetinaDisplayBitmap;}

    public Bitmap getSecondRetinaDisplayBitmap(){return mSecondRetinaDisplayBitmap;}

    public byte[] getAgencyLogoByteArray() {
        if (null != mAgencyLogoBitmap) {
            return getBytesFromBitmap(mAgencyLogoBitmap);
        }
        return null;
    }
    public byte[] getRetinaDisplayByteArray(){
        if (null != mRetinaDisplayBitmap) {
            return getBytesFromBitmap(mRetinaDisplayBitmap);
        }
        return null;

    }
    public byte[] getSecondRetinaDisplayByteArray(){
        if (null != mSecondRetinaDisplayBitmap) {
            return getBytesFromBitmap(mSecondRetinaDisplayBitmap);
        }
        return null;
    }
}

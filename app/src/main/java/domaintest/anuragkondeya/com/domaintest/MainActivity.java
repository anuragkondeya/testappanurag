package domaintest.anuragkondeya.com.domaintest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Activity to hold fragments for summary and detail views
 */
public class MainActivity extends AppCompatActivity implements
        PropertySummaryFragment.OnFragmentInteractionListener, Constants {

    PropertySummaryFragment summaryFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(null == savedInstanceState) {
            if (findViewById(R.id.property_summary) != null) {
                summaryFragment = new PropertySummaryFragment();
                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                summaryFragment.setRetainInstance(true);
                fragmentTransaction.add(R.id.property_summary, summaryFragment).commit();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.settings) {
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Property property) {
        if (findViewById(R.id.property_detail) == null) {
            if (findViewById(R.id.property_summary) != null) {
                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                final android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                PropertyDetailsFragment fragment = new PropertyDetailsFragment();
                Bundle extras = new Bundle();
                extras.putParcelable(PROPERTY_DETAILS_EXTRA, property);
                fragment.setArguments(extras);
                fragmentTransaction.replace(R.id.property_summary, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        } else {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            PropertyDetailsFragment fragment = new PropertyDetailsFragment();
            Bundle extras = new Bundle();
            extras.putParcelable(PROPERTY_DETAILS_EXTRA, property);
            fragment.setRetainInstance(true);
            fragment.setArguments(extras);
            fragmentTransaction.replace(R.id.property_detail, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }
}

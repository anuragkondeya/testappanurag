package domaintest.anuragkondeya.com.domaintest;

/**
 * Class to record all the constants in the project
 */
interface Constants {


    String URL_SCHEME_HTTPS         = "https";
    String URL_AUTHORITY            = "rest.domain.com.au";
    String URL_PATH_SEARCH_SERVICE  = "searchservice.svc";
    String URL_PATH_MAP_SEARCH      = "mapsearch";
    String URL_QPARAM_MODE          = "mode";
    String URL_QPARMA_SUBURB        = "sub";
    String URL_QPARAM_PINCODE       = "pcode";
    String URL_QPARAM_STATE         = "state";


    String  URL_QPARMA_MODE_DEFAULT      = "buy";
    String  URL_QPARMA_SUB_DEFAULT       = "Bondi";
    int     URL_QPARMA_PCODE_DEFAULT     = 2026;
    String  URL_QPARMA_STATE_DEFAULT     = "NSW";

    int PROPERTY_STANDARD                = 0;
    int PROPERTY_PREMIUM                 = 1;

    String SPACE                         = " ";
    String COMMA                         = ",";



    String DEBUG_URL_TEST = "https://rest.domain.com.au/searchservice.svc/mapsearch?mode=buy&sub=Bondi&pcodes=2026&state=NSW";

    String ADID                         =  "AdId";
    String AGENCY_LOGO_URL              =  "AgencyLogoUrl";
    String BATHROOMS                    =  "Bathrooms";
    String BEDROOMS                     =  "Bedrooms";
    String CARSPACE                     =  "Carspaces";
    String ISELITE                      =  "IsElite";
    String DISPLAY_PRICE                =  "DisplayPrice";
    String DISPLAYABLE_ADDRESS          =  "DisplayableAddress";
    String TRUNCATED_DESCRIPTION        = "TruncatedDescription";
    String RETINA_DISPLAY_URL           = "RetinaDisplayThumbUrl";
    String SECOND_RETINA_DISPLAY_URL    = "SecondRetinaDisplayThumbUrl";

    String LISTINGRESULTS               = "ListingResults";
    String LISTINGS                     = "Listings";


    String SETTING_PREFERENCE_MAIN      = "settings_key_main";
    String SETTINGS_KEY_MODE            = "settings_key_mode";
    String SETTINGS_KEY_SUB             = "settings_key_sub";
    String SETTINGS_KEY_PCODE           = "settings_key_pcode";
    String SETTINGS_KEY_STATE           = "settings_key_state";

    String PROPERTY_LIST_EXTRA            = "propertylistextra";
    String PROPERTY_DETAILS_EXTRA       = "propertydetailsextra";

}

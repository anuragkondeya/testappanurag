package domaintest.anuragkondeya.com.domaintest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Array adapter class for displaying standard listing
 */
class ListingAdapter extends BaseAdapter implements Constants {
    private ArrayList<Property> mPropertyList;
    private Activity mActivity;

    public ListingAdapter(Activity activity, ArrayList<Property> mPropertyList) {
        this.mActivity = activity;
        this.mPropertyList = mPropertyList;
    }

    @Override
    public int getCount() {
        return mPropertyList.size();
    }

    @Override
    public Object getItem(int i) {
        return mPropertyList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == mActivity) {
            return null;
        }
        if (null == mPropertyList){
            return null;
        }

        Context context = mActivity.getApplicationContext();
        Property property = mPropertyList.get(position);
        if (null != property) {
            switch (property.getIsElite()) {
                case PROPERTY_PREMIUM: {
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View premiumView = inflater.inflate(R.layout.layout_premium_listing, null);

                    ImageView premiumRetinaDisplay = (ImageView) premiumView.findViewById(R.id.premium_retina_display);
                    Bitmap premiumRetinaDisplayBitmap = property.getRetinaDisplayBitmap();
                    if(null!=premiumRetinaDisplayBitmap) {
                        premiumRetinaDisplay.setImageBitmap(premiumRetinaDisplayBitmap);
                    }


                    ImageView secondaryPremiumRetinaDisplay = (ImageView) premiumView.findViewById(R.id.premium_retina_display_secondary);
                    Bitmap secondaryPremiumRetinaDisplayBitmap = property.getSecondRetinaDisplayBitmap();
                    if(null!=secondaryPremiumRetinaDisplayBitmap) {
                        secondaryPremiumRetinaDisplay.setImageBitmap(secondaryPremiumRetinaDisplayBitmap);
                    }


                    TextView propertyPrice = (TextView) premiumView.findViewById(R.id.premium_property_price);
                    propertyPrice.setText(property.getDisplayPrice());

                    String BATH = mActivity.getResources().getString(R.string.bath);
                    String BED = mActivity.getResources().getString(R.string.bed);
                    String CAR = mActivity.getResources().getString(R.string.car);

                    String specifications  = property.getBedrooms()  + SPACE + BED + COMMA+SPACE+
                            property.getBathRooms() + SPACE + BATH +COMMA+SPACE+
                            property.getCarspace()  + SPACE + CAR;

                    TextView specificationsTV = (TextView) premiumView.findViewById(R.id.premium_property_specification);
                    specificationsTV.setText(specifications);

                    TextView address = (TextView) premiumView.findViewById(R.id.premium_property_price_address);
                    address.setText(property.getDisplayAddress());

                    ImageView agencyLogo = (ImageView) premiumView.findViewById(R.id.premium_agencylogo);
                    Bitmap agencyLogoBitmap = property.getAgencyLogoBitmap();
                    if(null!=agencyLogoBitmap) {
                        agencyLogo.setImageBitmap(agencyLogoBitmap);
                    }
                    return premiumView;
                }
                case PROPERTY_STANDARD:
                default: {
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View standardView = inflater.inflate(R.layout.layout_standard_listing, null);

                    ImageView standardThumnail = (ImageView) standardView.findViewById(R.id.standard_thumbnail);
                    Bitmap thumbnail = property.getRetinaDisplayBitmap();
                    if(null!=thumbnail) {
                        standardThumnail.setImageBitmap(thumbnail);
                    }

                    TextView banner = (TextView) standardView.findViewById(R.id.standard_banner);
                    banner.setText(" New ");  //Text hardcoded as payload doesnt include banner param

                    TextView propertyPrice = (TextView) standardView.findViewById(R.id.standard_property_price);
                    propertyPrice.setText(property.getDisplayPrice());

                    String BATH = mActivity.getResources().getString(R.string.bath);
                    String BED = mActivity.getResources().getString(R.string.bed);
                    String CAR = mActivity.getResources().getString(R.string.car);

                    String specifications  = property.getBedrooms()  + SPACE + BED + COMMA+SPACE+
                                             property.getBathRooms() + SPACE + BATH +COMMA+SPACE+
                                             property.getCarspace()  + SPACE + CAR;

                    TextView specificationsTV = (TextView) standardView.findViewById(R.id.standard_property_specification);
                    specificationsTV.setText(specifications);

                    TextView address = (TextView) standardView.findViewById(R.id.standard_property_price_address);
                    address.setText(property.getDisplayAddress());

                    ImageView agencyLogo = (ImageView) standardView.findViewById(R.id.standard_agencylogo);
                    Bitmap agencyLogoBitmap = property.getAgencyLogoBitmap();
                    if(null!=agencyLogoBitmap) {
                        agencyLogo.setImageBitmap(agencyLogoBitmap);
                    }
                    return standardView;
                }
            }
        }
        return null;
    }
}

package domaintest.anuragkondeya.com.domaintest;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

/**
 * Display image in fullscreen when clicked on details image fragment
 */
public class FullScreenImage extends Fragment implements Constants {
    Property mProperty;
    public FullScreenImage() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        mProperty = arguments.getParcelable(PROPERTY_DETAILS_EXTRA);
    }

    @Override
    public void onResume() {
        super.onResume();
        Activity activity = getActivity();
        Context context = activity.getApplicationContext();
        ImageButton imageButton = (ImageButton)activity.findViewById(R.id.imagefullscreen);
        imageButton.setImageBitmap(mProperty.getRetinaDisplayBitmap());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_full_screen_image, container, false);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

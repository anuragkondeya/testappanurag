package domaintest.anuragkondeya.com.domaintest;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

/**
 * Flash screen for branding
 */
public class FlashScreen extends Activity{
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_screen);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent intent = new Intent(FlashScreen.this,MainActivity.class);
                FlashScreen.this.startActivity(intent);
                FlashScreen.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }
}

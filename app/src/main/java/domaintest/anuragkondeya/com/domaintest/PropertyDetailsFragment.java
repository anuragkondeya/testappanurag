package domaintest.anuragkondeya.com.domaintest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Fragment displays details of a given property
 */
public class PropertyDetailsFragment extends Fragment implements Constants {
    Property mProperty;

    @Override
    public void onResume() {
        super.onResume();
        if (null != mProperty) {
            final Activity activity = getActivity();
            final Context context = activity.getApplicationContext();
            final ImageButton propertyImage = (ImageButton) getActivity().findViewById(R.id.detail_Image);
            propertyImage.setImageBitmap(mProperty.getRetinaDisplayBitmap());
            propertyImage.setOnClickListener(new ImageButton.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Activity activity = getActivity();
                    if (activity.findViewById(R.id.property_detail) == null) {
                        if (activity.findViewById(R.id.property_summary) != null) {
                            android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            FullScreenImage fragment = new FullScreenImage();
                            Bundle extras = new Bundle();
                            extras.putParcelable(PROPERTY_DETAILS_EXTRA, mProperty);
                            fragment.setArguments(extras);
                            fragmentTransaction.replace(R.id.property_summary, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        }
                    } else {
                        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        FullScreenImage fragment = new FullScreenImage();
                        Bundle extras = new Bundle();
                        extras.putParcelable(PROPERTY_DETAILS_EXTRA, mProperty);
                        fragment.setArguments(extras);
                        fragmentTransaction.replace(R.id.property_detail, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
                }
            });

            TextView propertyPrice = (TextView) getActivity().findViewById(R.id.detail_property_price);
            propertyPrice.setText(mProperty.getDisplayPrice());


            String BATH = activity.getResources().getString(R.string.bath);
            String BED = activity.getResources().getString(R.string.bed);
            String CAR = activity.getResources().getString(R.string.car);

            String specifications = mProperty.getBedrooms() + SPACE + BED + COMMA + SPACE +
                    mProperty.getBathRooms() + SPACE + BATH + COMMA + SPACE +
                    mProperty.getCarspace() + SPACE + CAR;

            TextView propertySpecification = (TextView) getActivity().findViewById(R.id.detail_property_specification);
            propertySpecification.setText(specifications);

            TextView propertyAddress = (TextView) getActivity().findViewById(R.id.detail_property_price_address);
            propertyAddress.setText(mProperty.getDisplayAddress());

            TextView propertyDescription = (TextView) getActivity().findViewById(R.id.detail_description);
            propertyDescription.setText(mProperty.getTruncatedDescription());
        }
    }

    public PropertyDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        mProperty = arguments.getParcelable(PROPERTY_DETAILS_EXTRA);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_property_details, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
